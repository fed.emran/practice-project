import { environment } from 'src/environments/environment';

/**
 * @description
 * AppConst contains all const properties of our application.
 * Some of the constent we can get from environment (Because, all constant might not be depends on envirment).
 * Some of them will be defined for all kinds of environment, like assets cdn or s3 url
 *
 * For new property, define property type in AppConsType and then add the property in APP_CONSTS
 */
const APP_CONSTS: AppConsType = {
  appBaseUrl: environment.appBaseUrl,
  apiBaseUrl: environment.apiBaseUrl,
  imageBaseUrl: environment.imageBaseUrl,
};

export default APP_CONSTS;

type AppConsType = {
  appBaseUrl: string;
  apiBaseUrl: string;
  imageBaseUrl: string;
};
