import { Pipe, PipeTransform } from '@angular/core';
import { Employee } from '../types/employee.interface';

@Pipe({
  name: 'searchFilter',
  standalone: true,
})
export class SearchPipe implements PipeTransform {
  transform(value: Array<Employee>, term: string | null) {
    return value.filter((item) => {
      if (term) {
        return (
          item.firstName.toLowerCase().includes(term) ||
          item.lastName.toLowerCase().includes(term)
        );
      } else {
        return item;
      }
    });
  }
}
