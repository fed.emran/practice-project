export interface Employee {
  id: number;
  firstName: string;
  lastName: string;
  age: number;
  gender: string;
  email: string;
  phone: string;
  birthDate: string;
  address: Address;
  company: Company;
}
interface Address {
  address: string;
  city: string;
}

interface Company {
  title: string;
}
