import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { EmployeeService } from './employee.service';
import { AgePipe } from './pipes/age.pipe';
import { SearchPipe } from './pipes/search.pipe';
import { Employee } from './types/employee.interface';
import { PaginationModule } from 'ngx-bootstrap/pagination';

@Component({
  selector: 'app-employee',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SearchPipe,
    AgePipe,
    PaginationModule,
  ],
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
})
export class EmployeeComponent implements OnInit {
  currentPage = 10;
  page!: number;
  totalItems: any;
  maxSize: number = 5;

  contentArray = new Array(30).fill('');

  dataSource: Array<Employee> = [];
  form = new FormGroup({
    search: new FormControl<string | null>(''),
  });

  constructor(private employeeService: EmployeeService) {}

  ngOnInit(): void {
    this.getAllEmployee();
  }
  getAllEmployee() {
    this.employeeService.getAllEmployee().subscribe((data: any) => {
      this.totalItems = data.total;
      this.dataSource = data.users;
      console.log(this.totalItems);
    });
  }

  pageChanged(event: any): void {
    this.page = event.page;
    console.log(this.page);
  }
}
