import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from './types/employee.interface';
import { BaseService } from 'src/app/shared/services/base.service';
import { SingleObjectOutput } from 'src/app/shared/types/single-object-output.interface';

@Injectable()
export class EmployeeService extends BaseService {
  constructor(injector: Injector) {
    super(injector);
  }

  getAllEmployee(): Observable<SingleObjectOutput<Employee>> {
    return this.http.get<SingleObjectOutput<Employee>>(
      `${this.apiUrl}/users`,
      this.httpOptions
    );
  }
}
