import { NumberTypeMonth, NumberTypeYear } from '../types/time.interface';

export abstract class Utils {
  public static validateEmail = (email: string): boolean => {
    const regularExpression =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regularExpression.test(String(email).toLowerCase());
  };
  public static getYears = (
    startYear: number = new Date().getFullYear(),
    limit: number = 50
  ): Array<NumberTypeYear> => {
    return Array(limit)
      .fill('#')
      .map((x, i) => {
        return { id: startYear + i, value: startYear + i };
      });
  };
  public static getMonth = (): Array<NumberTypeMonth> => {
    return Array(12)
      .fill('#')
      .map((x, i) => {
        return { id: i + 1, value: i + 1 };
      });
  };
  public static summerNoteConfig() {
    return {
      placeholder: '',
      tabsize: 2,
      height: 400,
      toolbar: [
        ['misc', ['codeview', 'undo', 'redo']],
        ['style', ['bold', 'italic', 'underline', 'clear', 'forecolor']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
      ],
      fontNames: ['Helvetica', 'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Roboto', 'Times'],
    };
  }
}
