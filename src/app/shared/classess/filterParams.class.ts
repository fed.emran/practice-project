interface ObjectMap {
  [key: string]: string;
}

export class FilterParams {
  private filterData: ObjectMap = {};

  get value() {
    return this.filterData;
  }

  set(key: string, value: string | number) {
    // @ts-ignore
    this.filterData[key] = value || typeof value === 'number' ? String(value) : null;
    return this;
  }

  setUsingObject(obj: ObjectMap) {
    Object.entries(obj).forEach(([key, value]) => this.set(key, value));
  }

  remove(key: string) {
    // @ts-ignore
    this.filterData[key] = null;
    return this;
  }

  /**
   * For Table Order Selected or Not
   *
   * @param key table Column key
   */
  isOrderSelected(key: string): boolean {
    const sortBy: string = this.value.sort_by;
    return sortBy ? sortBy.includes(key) : false;
  }

  setFilterFromQueryParams(data: ObjectMap) {
    this.reset();
    /**
     * Loop through all the key and set them to FilterData.
     */
    Object.keys(data).forEach((key) => this.set(key, data[key]));
  }

  /**
   * For Table Order ASC OR DSC
   *
   * @param key table Column key
   */
  // @ts-ignore
  order(key: string): 'asc' | 'dsc' {
    const sortBy = this.value.sort_by;
    if (sortBy) {
      const order = sortBy.substr(-3);
      const k = sortBy.replace('_' + order, '');
      return (key === k ? order : null) as 'asc' | 'dsc';
    }
  }

  reset() {
    this.filterData = {};
    return this.value;
  }
}
