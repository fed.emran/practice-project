/* tslint:disable:space-before-function-paren */
import { AbstractControl, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
export const emailRegex =
  /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const passwordPattern = '^(?!.* )(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[*\\-#@!/]).{0,}$';

export class Validation {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  static password = [Validators.required];
  // eslint-disable-next-line @typescript-eslint/unbound-method
  static email = [Validators.required, Validators.pattern(emailRegex)];

  /**
   * @description
   * compares the control value to the given controlName's value
   * gives error when both values are same
   * @usageNotes
   * ### validates 2 firstname and lastname are not same
   * ```typescript
   * const form = new FormGroup({
   *   firstname: new FormControl<string>('', [Validation.notSameAs('lastname')]),
   *   lastname: new FormControl<string>('', [Validation.notSameAs('firstname')]),
   * })
   * ```
   * @param controlName name of the other control you want to compare with
   * @returns A validator function that returns an error map with the
   * `notSameAs` property if the validation check fails, otherwise `null`.
   */
  static notSameAs(controlName: string): ValidatorFn {
    return ({ parent, value }: AbstractControl): ValidationErrors | null => {
      const otherValue = parent?.get(controlName)?.value;

      if (value && otherValue && value === otherValue) {
        return { notSameAs: { value } };
      }

      return null;
    };
  }

  /**
   * @description
   * check the value lenght of a control to given length
   * @usageNotes
   * ### Validates if the control value has 11 characters
   * ```typescript
   * const form = new FormGroup({
   *   mprn: new FormControl<string>('', [Validation.len(11)]),
   * })
   * ```
   * @param length control value's required length
   * @returns A validator function that returns an error map with the
   * `len` property if the validation check fails, otherwise `null`.
   */
  static len(length: number): ValidatorFn {
    return ({ value }: AbstractControl): ValidationErrors | null => {
      if (value && value.length !== length) {
        return { len: { value } };
      }

      return null;
    };
  }
  public static requiredIf(predicate: (control: AbstractControl) => boolean): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      // @ts-ignore
      if (control.parent && predicate(control.parent) && (control.value === null || control.value === undefined)) {
        return { required: { value: control.value } };
      }

      return null;
    };
  }
}
