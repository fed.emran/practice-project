import { Injector, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import AppConsts from 'src/app/app.const';

@Injectable()
export abstract class BaseService {
  httpOptions;
  formDataHttpOptions;
  params: HttpParams;
  protected http: HttpClient;

  constructor(injector: Injector) {
    this.params = new HttpParams();
    this.http = injector.get(HttpClient);
    this.httpOptions = {
      headers: new HttpHeaders({
        // eslint-disable-next-line @typescript-eslint/naming-convention
        'Content-Type': 'application/json',
      }),
    };

    this.formDataHttpOptions = {
      headers: new HttpHeaders({
        // 'Content-Type': 'application/x-www-form-urlencoded',
      }),
    };
  }

  get appUrl(): string {
    return AppConsts.appBaseUrl;
  }

  get apiUrl(): string {
    return AppConsts.apiBaseUrl;
  }

  get imageUrl(): string {
    return AppConsts.imageBaseUrl;
  }
}
