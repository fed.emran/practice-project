export interface ErrorResponse {
  // Error title
  title?: string;
  // Error message
  message?: string;
  errors?: Array<string>;
}
